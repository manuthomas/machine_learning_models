import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt

data = pd.read_csv("./data.csv" )
cost = []
def convert(row):
	if row.diagnosis=='M':
		return 1
	else:
		return 0

y = data.apply(convert,axis=1)
sc = StandardScaler()
x=sc.fit_transform(data.iloc[:,2:12])
x = np.c_[np.ones(len(x),dtype='int64'),x]
x_train,x_test,y_train,y_test = train_test_split(x,y,test_size = 0.2,random_state=42)

def sigmoid(z):
	return 1/(1+np.exp(-z))

def costFunction(x,y,theta):
	m,n = x.shape
	h = x.dot(theta)
	J = -1/m*np.sum(y*np.log(sigmoid(h))+(1-y)*np.log(1-sigmoid(h)))
	return J

def gradient(x,y,theta,alpha,iter):
	m,n = x.shape
	for i in range(iter):
		h = x.dot(theta)	
		cost.append(costFunction(x,y,theta))
		theta = theta - alpha/m*((x.T).dot(sigmoid(h)-y))
	return theta

def predict(x,theta):
	h = x.dot(theta)
	y = sigmoid(h)
	return (y>=0.5)
def checkCondn(row):
	if(row.loc[0]>=0.5):
		return 1
	else:
		return 0
theta = gradient(np.asarray(x_train),np.asarray(y_train),np.zeros(11),1,200)

y_pred = predict(np.asarray(x_test),theta)
acc = (y_pred == y_test)*100
print(acc.mean())

plt.plot(cost)
plt.xlabel("iteration")
plt.ylabel("cost")
plt.title("Cost Logistic")
plt.show()


#Logistic Regression using library

from sklearn import metrics
from sklearn.linear_model import LogisticRegression

log = LogisticRegression()
result = log.fit(x_train,y_train)
y_new = log.predict(x_test)
accuracy = metrics.accuracy_score(y_test, y_new)
accp = accuracy*100
print(accp)
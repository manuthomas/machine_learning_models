#!/usr/bin/env python
# coding: utf-8

# In[1]:


from sklearn.datasets import fetch_20newsgroups
from io import StringIO
import re
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk import pos_tag, ne_chunk, RegexpParser, ngrams
from nltk.corpus import wordnet, stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import os
from sklearn.metrics import homogeneity_score, v_measure_score
from sklearn.cluster import KMeans, MiniBatchKMeans
from IPython.display import display
from nltk.chunk.regexp import ChunkString, ChunkRule, ChinkRule
from nltk.tree import Tree
from nltk.chunk import RegexpChunkParser
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler


# In[36]:


#LOAD DATA FROM FILES

def loadData(categories):
    data = []
    for category in categories:
        files = ["20_newsgroups/" + category +"/" + x for x in os.listdir("20_newsgroups/" + category)]
        count=0
        for i in files:
            with open(i, mode='r', encoding='utf-8', errors='ignore') as file:
                ok = False
                text = file.read()
                if len(text) > 6000:
                    ok = True
                if ok:
                    data.append([text, category])
                    count += 1
            if count == 10:
                break

                
    
    
    return data


# In[3]:


#FUNCTION TO REMOVE NOISE

def noiseremoval(text):
    textstream = StringIO(text)
    while not "Lines" in textstream.readline():
        pass
    text = textstream.read()
   #text = re.sub('(---*)|>|<|#', '',text)
    text = re.sub(r'(\w)-(\w)', r'\g<1>\g<2>', text)
    text = re.sub('[^A-Za-z]', ' ', text)
    text = re.sub('\n|\t', ' ', text)
    text = re.sub('\ +', ' ', text)
   #print(text[1])
    return text


# In[4]:


#TOKENIZE DATA

def tokenize(text):
    words = word_tokenize(text)
    cleaned_words = list()
    for i in words:
        w = i.strip()
        if(len(w)>1):
            cleaned_words.append(w.lower())
    #print(cleaned_words)
    return cleaned_words


# In[5]:


#POS TAGGING

def get_wordnet_pos(treebank_tag):
        if treebank_tag.startswith('J'):
            return wordnet.ADJ
        elif treebank_tag.startswith('V'):
            return wordnet.VERB
        elif treebank_tag.startswith('N'):
            return wordnet.NOUN
        elif treebank_tag.startswith('R'):
            return wordnet.ADV
        else:
            return None


# In[6]:


#STEMMING AND LEMMATIZING

def stem_lemmatize_tagged(words_tagged):
        porter = PorterStemmer()
        lemmatizer = WordNetLemmatizer()
        words = words_tagged
        for i in range(len(words)):
            pos = get_wordnet_pos(words[i][1])
            if pos:
                words[i] = (lemmatizer.lemmatize(                                            porter.stem(words[i][0]),                                             pos=pos), words[i][1])
            else:
                words[i] = (lemmatizer.lemmatize(                                            porter.stem(words[i][0])), words[i][1])
        return words


# In[11]:


#TEXT PREPROCESSING


data = loadData([
        'alt.atheism',
        'comp.graphics',
        'sci.space'
    ])


# In[10]:


#FUNCTION TO DO PREPROCESSING ON TEXT
#1.NOISE REMOVAL
#2.TOKENIZATION
#3.POS TAGGING
#4.NGRAMS
#5.STEMMING AND LEMMATIZATION
#6.REMOVING STOP WORDS

def process(data):

    print("\n\n\n NOISE REMOVED \n\n\n")
    noisefree=[]
    for i in range(len(data)):
        noisefree.append(noiseremoval(data[i][0]))
    print(noisefree[1])




    print("\n\n\n TOKENIZED \n\n\n")

    tokenized = []
    for i in range(len(noisefree)):
        tokenized.append(tokenize(noisefree[i]))

    for i in tokenized[1]:
        print(i)





    print("\n\n\n POS Tagged \n\n\n")


    pos_tagged = []
    for i in range(len(tokenized)):
        pos_tagged.append(pos_tag(tokenized[i]))
        #print(pos_tagged[i])
    for i in pos_tagged[1]:
         print(i[1], "\t", i[0])  





    print("\n\n\n Ngrams \n\n\n")        

    news_2_grams = []
    for i in range(len(tokenized)):
        news_2_grams.append(ngrams(tokenized[i], 2))

    for i in news_2_grams[1]:
        print(i)






    print("\n\n\n Stem lemmatied \n\n\n")

    stem_lemm = []
    for i in range(len(pos_tagged)):
        stem_lemm.append(stem_lemmatize_tagged(pos_tagged[i]))
    for i in stem_lemm[1]:
        print(i[0])






    print("\n\n\n Stop Word Removed \n\n\n")      

    stopWords = set(stopwords.words('english'))
    stopWordFree = []
    for i in range(len(stem_lemm)):
        removed = []
        for j in stem_lemm[i]:
            if len(j[0]) > 1 and j[0] not in stopWords:
                removed.append(j)
        stopWordFree.append(removed)
    for i in stopWordFree[1]:
        print(i[0])


    return stopWordFree


# In[9]:

print("\n\n\n\t\t TEXT PREPROCESSING \n\n\n")


stopWordFree = process(data)


#CHUNKING OF STOP WORD FREE TEXT



print("\n\n\n Chunked \n\n\n")

chunked=[]
tree = Tree('S', [x for x in stopWordFree[0]])
for i in range(len(stopWordFree)):
    tree = Tree('S', [x for x in stopWordFree[i]])
    reg = r"""
        NN: {<JJ.*><NN.*>+|<NN.*><NN.*>+}
        VB: {<RB.*><VB.*>}
        """
    parser = RegexpParser(reg)
    chunked.append(parser.parse(tree))
    
type_tree = type(Tree('S', []))
for chunk in chunked[1]:
    if(type(chunk) == type_tree):
        display(chunk)

        
        


# In[37]:
print("\n\n\n\t\t CLUSTERING\n\n\n")

cluster_data = loadData([
        'comp.sys.mac.hardware',
        'talk.politics.misc',
        'sci.space',
        'rec.sport.baseball',
        'rec.autos'
    ])
print(len(cluster_data))
labels = [x[1] for x in cluster_data]
news = [x[0] for x in cluster_data]


# In[38]:


news_processed = process(cluster_data)


# In[44]:


news_processed_str = []
for i in range(len(news_processed)):
    tag_stripped = [x[0] for x in news_processed[i]]
    news_processed_str.append(' '.join(tag_stripped))


# In[50]:


#USED HYPERPARAMETERS ARE:
#{'ngram_range': (0, 7), 'use_idf': True, 'df_min_max': (0.0, 0.7), 'norm': 'l2'}


y = labels
hyperparams = {'ngram_range': (0, 7), 'use_idf': True, 'df_min_max': (0.0, 0.7), 'norm': 'l2'}
vectorizer = TfidfVectorizer(ngram_range=hyperparams["ngram_range"],
                                 stop_words='english',
                                 use_idf=hyperparams["use_idf"],
                                 strip_accents='ascii',
                                 lowercase=True,
                                 max_df=hyperparams["df_min_max"][1],
                                 min_df=hyperparams["df_min_max"][0],
                                 norm=hyperparams["norm"],
                            )
X = vectorizer.fit_transform(news_processed_str)
X = np.array(X.toarray())
km = KMeans(n_clusters=5, max_iter=100, random_state=0)
km.fit(X)
y_pred = km.predict(X)
print("\n\n\n\t\t Scores \n\n\n")
print("Homogeneity Score\t: %.3f" % homogeneity_score(y, y_pred))
print("V Score\t\t\t: %.3f" % v_measure_score(y, y_pred))
lim = 10
homo_scores = []
v_scores = []
wcss_errs = []
for n_clusters in range(1, lim): 
    km = KMeans(n_clusters=n_clusters, max_iter=100, random_state=0)
    km.fit(X)
    y_pred = km.predict(X)
    wcss_errs.append(km.inertia_)
    homo_scores.append(homogeneity_score(y, y_pred))
    v_scores.append(v_measure_score(y, y_pred))
plt.plot(range(1, lim), [x for x in wcss_errs], c='blue')
plt.xlabel("Iter")
plt.ylabel("wcss_errs")
plt.savefig('WCSS.png')
plt.show()


import pandas as pd
import numpy as np
from sklearn import tree
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
data1=pd.read_csv('dermatology.data',header=None,sep=',')
temp=data1.values
mask=np.any(np.equal(temp,'?'), axis=1)
temp=temp[~mask]
data=pd.DataFrame(temp)
X=temp[:,:34]
Y=temp[:,-1]
sc=StandardScaler()
Y=Y.astype('int')
X=sc.fit_transform(X)
X_train, X_test, Y_train, Y_test=train_test_split(X,Y, test_size=0.2,random_state=1)

print('\n','############## Logistic Regression ##############','\n')


logreg=LogisticRegression()
print(logreg.fit(X_train,Y_train))
print('Accuracy: ', (logreg.score(X_test,Y_test)))


print('\n','############## Decision Tree Classifier ##############','\n')


dectree=DecisionTreeClassifier(max_depth=20,min_samples_leaf=1)
dtree=dectree.fit(X_train,Y_train)
y_pred0 = dectree.predict(X_test)
print(confusion_matrix(Y_test,y_pred0))
print('Accuracy: ', accuracy_score(Y_test, y_pred0))



print('\n','############## Support Vector Machine ##############','\n')

svc1=SVC(kernel='linear')
print(svc1.fit(X_train,Y_train))
y_pred1=svc1.predict(X_test)
print('\n',confusion_matrix(Y_test,y_pred1))
print('\n\n',classification_report(Y_test,y_pred1))
print('Accuracy: ', accuracy_score(Y_test,y_pred1))



print('\n','############## Random Forest ##############','\n')
RFclassifier= RandomForestClassifier(n_estimators=20, criterion='gini', random_state=1, max_depth=20)
RFclassifier.fit(X_train,Y_train)
y_pred=RFclassifier.predict(X_test)
print('\n',confusion_matrix(Y_test,y_pred))
print('\n',classification_report(Y_test, y_pred))
print('Accuracy: ', accuracy_score(Y_test, y_pred))
import pandas as pd
import numpy as np
import itertools


def entropy(target_col):
    elements, counts = np.unique(target_col, return_counts=True)

    entropy = np.sum(
        [(-counts[i] / np.sum(counts)) * np.log2(counts[i] / np.sum(counts)) for i in range(len(elements))])
    return entropy


def InfoGain(data, split_attribute_name, target_name):
    # split_attribute_name = the name of the feature for which the information gain should be calculated
    # target_name = the name of the target feature

    # Calculate the entropy of the total dataset
    total_entropy = entropy(data[target_name])

    # Calculate the values and the corresponding counts for the split attribute
    vals, counts = np.unique(data[split_attribute_name], return_counts=True)

    # Calculate the weighted entropy
    Weighted_Entropy = np.sum(
        [(counts[i] / np.sum(counts)) * entropy(data.where(data[split_attribute_name] == vals[i]).dropna()[target_name])
         for i in range(len(vals))])

    # Calculate the information gain
    Information_Gain = total_entropy - Weighted_Entropy
    return Information_Gain


names = ['erythema', 'scaling', 'definite borders', 'itching', 'koebner phenomenon', 'polygonal papules',
         'follicular papules', 'oral mucosal involvement', 'knee and elbow involvement', 'scalp involvement',
         'family history', 'melanin incontinence', 'eosinophils in the infiltrate', 'PNL infiltrate',
         'fibrosis of the papillary dermis', 'exocytosis', 'acanthosis', 'hyperkeratosis', 'parakeratosis',
         'clubbing of the rete ridges', 'elongation of the rete ridges', 'thinning of the suprapapillary epidermis',
         'spongiform pustule', 'munro microabcess', 'focal hypergranulosis', 'disappearance of the granular layer',
         'vacuolisation and damage of basal layer', 'spongiosis', ' saw-tooth appearance of retes',
         'follicular horn plug', 'perifollicular parakeratosis', 'inflammatory monoluclear inflitrate',
         'band-like infiltrate', 'Age', 'val']
data = pd.read_csv(r'dermatology.data', names=names)

# print(data.iloc[250:300,0:34])
mask=np.any(np.equal(data,'?'), axis=1)
data=data[~mask]
d = {}
for i in range(34):
    attr = names[i]
    target = 'val'
    d[attr] = InfoGain(data, attr, target)
a = sorted(d.items(), key=lambda x: x[1], reverse=True)
final=a[:10]   # best 10 features
for items in final:
    print(items)


import pandas as pd
import numpy as np
import statsmodels.api as sm

def forward_selection(data, target, significance_level=0.05):
    initial_features = data.columns.tolist()
    best_features = []
    while (len(list(initial_features))>0):
        remaining_features = list(set(initial_features)-set(best_features))
        new_pval = pd.Series(index=remaining_features)
        for new_column in remaining_features:
            #est = sm.OLS(y, X.astype(float)).fit()
            model = sm.OLS(target, (sm.add_constant(data[best_features+[new_column]])).astype(float)).fit()
            new_pval[new_column] = model.pvalues[new_column]
     
        min_p_value = new_pval.min()
        if(min_p_value<significance_level):
            best_features.append(new_pval.idxmin())
        else:
            break
    return best_features

# load data
names = ['erythema', 'scaling', 'definite borders', 'itching', 'koebner phenomenon', 'polygonal papules', 'follicular papules', 'oral mucosal involvement', 'knee and elbow involvement','scalp involvement','family history','melanin incontinence','eosinophils in the infiltrate','PNL infiltrate','fibrosis of the papillary dermis','exocytosis','acanthosis','hyperkeratosis','parakeratosis','clubbing of the rete ridges','elongation of the rete ridges','thinning of the suprapapillary epidermis','spongiform pustule','munro microabcess','focal hypergranulosis','disappearance of the granular layer','vacuolisation and damage of basal layer','spongiosis',' saw-tooth appearance of retes','follicular horn plug','perifollicular parakeratosis','inflammatory monoluclear inflitrate','band-like infiltrate','Age','val']
#data = pd.read_csv(r"dermatology.data",names=names)
data = pd.read_csv(r"dermatology.data",names=names)

#removing incomplete rows
mask=np.any(np.equal(data,'?'), axis=1)
data=data[~mask]

X = data.iloc[:,0:34]  #independent columns
y = data.iloc[:,-1]    #target column

final = forward_selection(X,y)
for item in final:
    print(item)
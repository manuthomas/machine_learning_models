from sklearn.feature_selection import VarianceThreshold,mutual_info_classif
import numpy as np
import statistics as stat
from  sklearn.preprocessing import StandardScaler
import pandas as pd
data1=pd.read_csv('dermatology.data',header=None,sep=',')
temp=data1.values
data2=temp[:,:34]
y=temp[:,-1]
sc=StandardScaler()
print("\n\nNo. of Data entries pre Processing:",len(data2))
count=0
mask=np.any(np.equal(data2,'?'), axis=1)
data2=data2[~mask]
print("No of non-numeric rows:",len(mask)-len(data2))
print("No. of Data entries post processing:",len(data2))
data2=data2.astype(int)

data4=pd.DataFrame(data2)
for c in  data4.columns.tolist():
    varianceNb=stat.variance(data4[c])
    print(c,"col. variance: ",varianceNb)
sel=VarianceThreshold(threshold=(.8*(1-.8)))
data3=sel.fit_transform(data2)
print("\nShape of original numpy array",data2.shape)
print("Number of Columns to be removed:",data2.shape[1]-data3.shape[1])
print("Shape of numpy array after removal",data3.shape)


import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
# load data
names = ['erythema', 'scaling', 'definite borders', 'itching', 'koebner phenomenon', 'polygonal papules', 'follicular papules', 'oral mucosal involvement', 'knee and elbow involvement','scalp involvement','family history','melanin incontinence','eosinophils in the infiltrate','PNL infiltrate','fibrosis of the papillary dermis','exocytosis','acanthosis','hyperkeratosis','parakeratosis','clubbing of the rete ridges','elongation of the rete ridges','thinning of the suprapapillary epidermis','spongiform pustule','munro microabcess','focal hypergranulosis','disappearance of the granular layer','vacuolisation and damage of basal layer','spongiosis',' saw-tooth appearance of retes','follicular horn plug','perifollicular parakeratosis','inflammatory monoluclear inflitrate','band-like infiltrate','Age','val']
data = pd.read_csv(r"dermatology.data",names=names)

#removing incomplete rows
mask=np.any(np.equal(data,'?'), axis=1)
data=data[~mask]

array = data.values
X = array[:,0:34]
Y = array[:,34]
# feature extraction. Specify no of principal components here n = 3
pca = PCA(n_components=3)
fit = pca.fit(X)
print(fit.components_)

# summarize components
print("Explained Variance: %s" % fit.explained_variance_ratio_)
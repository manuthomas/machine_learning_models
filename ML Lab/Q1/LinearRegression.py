import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

df = pd.read_csv("airfoil_self_noise.dat",header=None,sep="\t")
print(df.shape)

def cost_function(X, Y, B):
   m=len(Y)
   J=np.sum((X.dot(B)-Y)**2)/(2*m)
   return J

def plotFunc(plot_x,plot_y):
	plt.plot(plot_x, plot_y)
	plt.xlabel('Iterations')
	plt.ylabel('cost')
	plt.show()
	print("Initial Cost: ",plot_y[0],"\n Final cost: ",plot_y[-1])

def gradient_descent(X, Y, B, alpha, iterations):
    cost_history = [0] * iterations
    m = len(Y)
    plot_x = []
    plot_y = []
    count=0
    for iteration in range(iterations):
        plot_x.append(iteration)
        h = X.dot(B)
        loss = h - Y
        gradient = X.T.dot(loss) / m
        B = B - alpha*gradient
        cost = cost_function(X, Y, B)
        cost_history[iteration] = cost
        plot_y.append(cost)
    plotFunc(plot_x,plot_y)
    
    return B, cost_history



X=df.iloc[:, :5]
Y=df.iloc[:, -1]
sc = StandardScaler()

#print(Y.iloc[0],X.iloc[0])

testcount = 1300
colSize = 5

X_train=X.iloc[:testcount,:colSize]
X_train=sc.fit_transform(X_train)
X_train=np.c_[np.ones(len(X_train),dtype='int64'),X_train]

Y_train=Y.iloc[:testcount]

Y_test=Y.iloc[testcount:]

X_test=X.iloc[testcount:,:colSize]
X_test=sc.fit_transform(X_test)
X_test=np.c_[np.ones(len(X_test),dtype='int64'),X_test]

B = np.zeros(X_train.shape[1])

alpha = 0.001
iterations =10000

newB, cost_history = gradient_descent(X_train, Y_train, B, alpha, iterations)
#print(newB)
#print(cost_history[-1])

def r2Score(y_,y):
    sst=np.sum((y-y.mean())**2)
    ssr=np.sum((y_-y)**2)
    r2=1-(ssr/sst)
    return(r2)

def predicted(X_test,newB):
    return X_test.dot(newB)

y_=predicted(X_test,newB)
print(r2Score(y_,Y_test))


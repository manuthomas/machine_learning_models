import random
import numpy as np
import copy
import time
from matplotlib import pyplot as plt

h=974
random.seed(h)
np.random.seed(h)
cross_p=.8
n_matings=10
n_iterations=50
mutation_threshold=.01

def to_binary(i,n):
    b= '{0:b}'.format(i)
    while len(b) < n:
        b='0' + b
    return b


def to_int(b):
    return int(b,2)


def decision(probability):
    return int(random.random()<probability)


def crossover(i1,i2):
    assert len(i1)==len(i2)
    n= len(i1)
    split_index= random.randint(0+1,n-1)
    if(decision(cross_p)):
        return '{}{}'.format(i1[:split_index], i2[split_index:]),'{}{}'.format(i1[split_index:], i2[:split_index])
    else:
        return i1,i2


def mutate(i1, threshold):
    def swap_bitc(c):
        if c=='0':
            return '1'
        return '0'
    return ''.join([swap_bitc(c) if random.uniform(0,1) <= threshold else c for c in i1])


def ranking_population(individuals):
    def calc_fitness(i1):
        return 26213-abs(to_int(i1)**3 + 9)
    return sorted([i for i in individuals],key=lambda x: x[1], reverse=True)

n=6
min_val=to_int('0'*n)
max_val=to_int('1'*n)
n_individuals=10
individual_indexes=list(range(0,n_individuals))
population=[to_binary(random.randint(min_val,int(max_val/2)),6) for i in range(0,n_individuals)]
k=[]

for items in population:
    k.append(to_int(items))
k=sorted(k)
print("starting: ",k)

count=1
count2=0
iter_list=[]
best_child=[]
iter_list.append(count)
best_child.append(k[-1])

for iteration in range(0,n_iterations):
    population_int = []
    items_to_mate=[]
    ratio=[]
    population_int2=[]
    new_population= population.copy()

    for _ in range(0, n_matings):
        temp2=sorted(set(population))

        for items in temp2:
            population_int2.append((to_int(items))**3)

        population_int2=population_int2
        temp1=population_int2
        sum_parent=sum(temp1)

        for items in temp1:
            ratio.append(items/sum_parent)

        len_ratio=len(ratio)
        out=0
        while(1):
            p = random.uniform(0, 1)
            for i in range(0,len_ratio):
                if(p<=ratio[i]):
                    items_to_mate.append(i)
                    out=1
                    break
            if(out==1):
                break

        out=0
        while(1):
            p = random.uniform(0,1)
            for i in range(0,len_ratio):
                if(p<=ratio[i]):
                    items_to_mate.append(i)
                    out=1
                    break
            if(out==1):
                break

        i1,i2=crossover(population[items_to_mate[0]],population[items_to_mate[1]])
        new_population.append(i1)
        new_population.append(i2)

    for individual in new_population:
        individual=mutate(individual, mutation_threshold)
    ranked_population = ranking_population(new_population)

    if(to_int(ranked_population[-1]) == 63):
        count2+=1
        if(count2==11):
            break

    sort_pop=copy.deepcopy(list(sorted(population)))
    ranked_population=sorted(ranked_population,reverse=True)
    leng=len(ranked_population)
    use=int(leng/5)

    for u in range(0,use):
        ranked_population[leng-1-u]=sort_pop[-1]
        sort_pop.pop()

    a=random.uniform(0,1)
    population=ranked_population
    count+=1

    for items in population:
        population_int.append(to_int(items))

    population_int=sorted(set(population_int))
    print("size:",len(population),"   population:",population_int)
    iter_list.append(count)
    best_child.append(((max(set(population_int)))**3+9)/1000)

plt.plot(iter_list,best_child)
plt.xlabel('iterations')
plt.ylabel('Fitness Function(in thousands)')
plt.show()
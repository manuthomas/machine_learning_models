import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv("iris.data",header=None,sep=",")
print(data.shape)

instances = len(data.index)

k_array = np.arange(2,6,1)


classes = data.iloc[:,4].unique() 

WCSS_array = np.array([])




irisDataframe = data.iloc[:,0:4]
irisArray = irisDataframe.to_numpy(copy=True)

data= data.reindex(
    	columns=[*data.columns.tolist(
      	),5])

for k in k_array:
	
	print(k)
	centroidsFixed=False
	iterLimit = 300
	clusterAssigned = np.empty(irisArray.shape[0])
	data[5] = clusterAssigned
	centroids = irisArray[np.random.choice(irisArray.shape[0],size=k,replace = False),:]
	distances = np.empty((instances,k))
	c=0
	errdist = np.array([])
	iternum = np.array([])
	while iterLimit>0 and not(centroidsFixed):
		
		#calculating min dist
		for i in range(k):
			distances[:,i] = np.linalg.norm(irisArray - centroids[i],axis=1	)
		clusterAssigned = np.argmin(distances,axis=1)
		errdist = np.append(errdist, np.sum(np.min(distances,axis=1)))
		iternum = np.append(iternum, c)
		print(errdist)
		c+=1
		
	#updating centroids
		centroids_old = centroids.copy()
		for i in range(k):
			centroids[i] = np.mean(irisArray[clusterAssigned==i],axis =0)

		centroidsFixed = np.array_equal(centroids_old,centroids)	
		if(centroidsFixed):
			print(iterLimit)
			print("Final centroids achieved")
			print(centroids)
		iterLimit-=1	


		data[5] = clusterAssigned


	distance = np.zeros((instances,1))
	for j in range(instances):
		i=clusterAssigned[j]

		distance[j] = ((irisArray[j][0]-centroids[i][0])**2) + ((irisArray[j][1] - centroids[i][1])**2) + ((irisArray[j][2] - centroids[i][2])**2) + ((irisArray[j][3] - centroids[i][3])**2)

	sum = 0

	for j in range(len(distance)):
		sum = sum + distance[j]
	WCSS_array=np.append(WCSS_array,sum)

	if(k==3):
		colors1 = ['orange', 'blue', 'green']
		for i in range(instances):
			plt.scatter(irisArray[i, 0], irisArray[i, 1], s=7, color=colors1[int(clusterAssigned[i])])
		for j in range(0,3):
			plt.scatter(centroids[j, 0], centroids[j, 1], marker='*', color='red', s=150)
		plt.show()

		plt.plot(iternum,errdist)
		plt.title("Min Distance over iteration For k=3")
		plt.show()

		


print(WCSS_array)

plt.plot(k_array,WCSS_array)
plt.xlabel("no of clusters")
plt.ylabel("WCSS")
plt.title("Elbow Method")
plt.show()


